var dolma = $('input[type="hidden"][name="dolma"]').attr('data-skus').split(';');
var calça = $('input[type="hidden"][name="calça"]').attr('data-skus').split(';');
var avental = $('input[type="hidden"][name="avental"]').attr('data-skus').split(';');
var touca = $('input[type="hidden"][name="touca"]').attr('data-skus').split(';');
var sapato = $('input[type="hidden"][name="sapato"]').attr('data-skus').split(';');
var bandana = $('input[type="hidden"][name="bandana"]').attr('data-skus').split(';');
var lenco = $('input[type="hidden"][name="lencos"]').attr('data-skus').split(';');

var valorDolma = 0;
var valorCalça = 0;
var valorAvental = 0;
var valorSapato = 0;
var valorTouca = 0;
var valorBandana = 0;
var valorLenco = 0;
var valorTotal = 0;
var valorParcela = 0;

function createSelections(classe, nome, size, arrayProd){
    content = '';
    if(size){
        content += `<div class="selection ${classe} have_sizes">`;
    }else{
        content += `<div class="selection ${classe}">`;
    }
        content += '<div class="input">';
            content += `<p>${nome}</p>`;
            content += `<select class="prod_name" id="${classe}"><option value="" selected disabled hidden>Selecione</option></select>`;
        content += '</div>';
        if(size){
            content += '<div class="input">';
                content += '<p>Tamanho</p>';
                content += `<select class="prod_size" id="${classe}"></select>`;
            content += '</div>';
        }
        content += '<div class="quantity">';
            content += '<p>Quantidade</p>';
            content += '<div class="qnt_input">';
            content += '<button class="minus">-</button>';
            content += '<input type="number" min="0" value="1" disabled>';
            content += '<button class="plus">+</button>';
            content += '</div>';
        content += '</div>';
    content += '</div>';
    $('main.kit_page .display_kit').append(content);
    createOptions(classe, size, arrayProd);
}

function createOptions(selection, size, arrayProd){
    arrayProd.forEach(element => {
        $.ajax({
            url: "/api/catalog_system/pub/products/search?fq=productId:" + element,
            type: 'GET',
            headers: header
        }).
        done(function (response) {
            var prodName = response[0].productName;
            var prodID = response[0].productId;
            var prodSku0 = response[0].items[0].itemId;
            var prodPrice = response[0].items[0].sellers[0].commertialOffer.Price;
            var quantityMax = response[0].items[0].sellers[0].commertialOffer.AvailableQuantity;
            var Image = response[0].items[0].images[0].imageUrl;
            $('.display_kit-images .slider-for').append(`<li><img src="${Image}"></li>`);
            $('.display_kit-images .slider-nav').append(`<li><img src="${Image}"></li>`);
            
            if (prodName == 'Sapato Preto Profissional Antiderrapante') prodName = 'Tênis Profissional Preto Antiderrapante';

            if(size){
                contentOption = `<option value="${prodID}">${prodName}</option>`;
            }else{
                contentOption = `<option value="${prodSku0}" data-price="${prodPrice}" data-qntMax="${quantityMax}">${prodName}</option>`;
            }
            $(`.${selection} .prod_name`).append(contentOption);
        }); 
    });
}

function loadSizes(selection, productId){
    $(`.${selection} .prod_size`).html('<option value="" selected disabled hidden>Selecione</option>'); // Reset Options
    $.ajax({
        url: "/api/catalog_system/pub/products/search?fq=productId:" + productId,
        type: 'GET',
        headers: header
    }).
    done(function (response) {
        var sizes = response[0].items;
        sizes.forEach(element => {
            var sizeName = element.name;
            var itemId = element.itemId;
            var quantityMax = element.sellers[0].commertialOffer.AvailableQuantity;
            var prodPrice = element.sellers[0].commertialOffer.Price;
            if(quantityMax > 0){
                contentOption = `<option value="${itemId}" data-price="${prodPrice}" data-qntMax="${quantityMax}">${sizeName}</option>`;
                $(`.${selection} .prod_size`).append(contentOption);
            }
        });
    }); 
}

function CreateUrl() {
    var pDolma = '';
    var pCalça = '';
    var pAvental = '';
    var pTouca = '';
    var pSapato = '';
    var pBandana = '';
    var pLenco = '';

    if($('#selection_01.prod_size option:selected').val() !== '' && $('#selection_01.prod_size option:selected').val() !== undefined){
        pdolmaSku = $('#selection_01.prod_size option:selected').val();
        pdolmaQnt= $('.selection_01 input[type="number"]').val();
        pDolma = `sku=${pdolmaSku}&qty=${pdolmaQnt}&seller=1&redirect=true&`;
    }
    if($('#selection_02.prod_size option:selected').val() !== '' && $('#selection_02.prod_size option:selected').val() !== undefined){
        pcalçaSku = $('#selection_02.prod_size option:selected').val();
        pcalçaQnt= $('.selection_02 input[type="number"]').val();
        pCalça = `sku=${pcalçaSku}&qty=${pcalçaQnt}&seller=1&redirect=true&`;
    }
    if($('#selection_03.prod_name option:selected').val() !== '' && $('#selection_03.prod_name option:selected').val() !== undefined){
        paventalSku = $('#selection_03.prod_name option:selected').val();
        paventalQnt= $('.selection_03 input[type="number"]').val();
        pAvental = `sku=${paventalSku}&qty=${paventalQnt}&seller=1&redirect=true&`;
    }
    if($('#selection_04.prod_name option:selected').val() !== '' && $('#selection_04.prod_name option:selected').val() !== undefined){
        ptoucaSku = $('#selection_04.prod_name option:selected').val();
        ptoucaQnt= $('.selection_04 input[type="number"]').val();
        pTouca = `sku=${ptoucaSku}&qty=${ptoucaQnt}&seller=1&redirect=true&`;
    }
    if($('#selection_05.prod_size option:selected').val() !== '' && $('#selection_05.prod_size option:selected').val() !== undefined){
        pSapatoSku = $('#selection_05.prod_size option:selected').val();
        pSapatoQnt= $('.selection_05 input[type="number"]').val();
        pSapato = `sku=${pSapatoSku}&qty=${pSapatoQnt}&seller=1&redirect=true&`;
    }
    if($('#selection_06.prod_name option:selected').val() !== '' && $('#selection_06.prod_name option:selected').val() !== undefined){
        pbandenaSku = $('#selection_06.prod_name option:selected').val();
        pbandenaQnt= $('.selection_06 input[type="number"]').val();
        pBandana = `sku=${pbandenaSku}&qty=${pbandenaQnt}&seller=1&redirect=true&`;
    }
    if($('#selection_07.prod_name option:selected').val() !== '' && $('#selection_07.prod_name option:selected').val() !== undefined){
        plencoSku = $('#selection_07.prod_name option:selected').val();
        plencoQnt= $('.selection_07 input[type="number"]').val();
        pLenco = `sku=${plencoSku}&qty=${plencoQnt}&seller=1&redirect=true&`;
    }

    var newHref = 'https://www.reidacutelaria.com.br/checkout/cart/add?' + pDolma + pCalça + pAvental + pTouca + pSapato + pBandana;
    $('.buy_button a').attr('href', newHref);
}

function calculatePrice(){
    resultValorDolma = valorDolma * $('.selection.selection_01 .quantity input[type="number"]').val();
    resultValorCalça = valorCalça * $('.selection.selection_02 .quantity input[type="number"]').val();
    resultValorAvental = valorAvental * $('.selection.selection_03 .quantity input[type="number"]').val();
    resultValorTouca = valorTouca * $('.selection.selection_04 .quantity input[type="number"]').val();
    resultValorSapato = valorSapato * $('.selection.selection_05 .quantity input[type="number"]').val();
    resultValorBandana = valorBandana * $('.selection.selection_06 .quantity input[type="number"]').val();
    resultValorLenco = valorLenco * $('.selection.selection_07 .quantity input[type="number"]').val();
    
    valorTotal = resultValorDolma + resultValorCalça + resultValorAvental + resultValorTouca + resultValorSapato + resultValorBandana + resultValorLenco;
    valorParcela = valorTotal / 10;
    valorTotal = Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(valorTotal)
    $('main.kit_page .price_display span').html(valorTotal);
    valorParcela = Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(valorParcela)
    $('main.kit_page .parcela_display p span').html(valorParcela);
    //Criando URL
    CreateUrl();
}

function listFaculdades(){
    $(`.display_faculdade`).append('<div class="input-check"><input type="checkbox" id="nao_cadastrada"><p>Faculdade não cadastrada.</p></div>');
    $(`.display_faculdade`).append('<p class="obs_logo">Outras faculdades, sob consulta.<img src="../../../arquivos/logo-whatsapp.png" /><a class="whatsapp-kit" href="https://api.whatsapp.com/send?phone=5511947902921" target="_blank">+55 (11) 94790-2921</a></p>');
    $(`.display_faculdade select`).html('<option value="" selected disabled hidden>Selecione</option>');
    $.ajax({
        url: '/api/dataentities/GN/search?_fields=faculdade,link,logo,id&_sort=faculdade ASC',
        type: 'GET',
        headers: header
    }).
    done(function (res) {
        var index = 0;
        res.forEach(element => {
            index++;
            if(index > 1){
                contentOption = `<option value="${element.faculdade}">${element.faculdade}</option>`;
                $(`.display_faculdade .select_faculdade`).append(contentOption);
            }
        });
    })
}

function createDescription(array, id){
    // Dolmãs
    array.forEach(element => {
        $.ajax({
            url: "/api/catalog_system/pub/products/search?fq=productId:" + element,
            type: 'GET',
            headers: header
        }).
        done(function (response) {  
            $(`.display_tabs #${id}`).append(response[0].description);
        })

    })
}

function initDescription(){
    $('.tabs span').click(function (e) { 
        var tab_ref =$(this).attr('data-ref');
        $('.tabs span, .display_tabs .ref_tab').removeClass('actived')
        $(this).addClass('actived')
        $(`.display_tabs #${tab_ref}`).addClass('actived');
    });

    createDescription(dolma, 'dolma');
    createDescription(calça, 'calca');
    createDescription(avental, 'avental');
    createDescription(touca, 'touca');
    createDescription(sapato, 'sapato');
    createDescription(bandana, 'bandana');
    createDescription(lenco, 'lenco');
}

$(document).ready(function () {
    $('.display_kit-images').append('<ul class="slider-for"></ul><ul class="slider-nav"></ul>')
    listFaculdades();
    valorTotal = Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(valorTotal)
    $('main.kit_page .price_display span').html(valorTotal);
    valorParcela = Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(valorParcela)
    $('main.kit_page .parcela_display p span').html(valorParcela);
    //Create Dolmã
    createSelections('selection_01', 'Dolmã', true, dolma);

    //Create Calça
    createSelections('selection_02', 'Calça', true, calça);

    //Create Avental
    createSelections('selection_03', 'Avental', false, avental);

    //Create Touca
    createSelections('selection_04', 'Touca', false, touca);

    //Create Dolmã
    createSelections('selection_05', 'Sapato', true, sapato);

    //Create Bandana
    createSelections('selection_06', 'Bandana', false, bandana);
    
    //Create Lenço
    createSelections('selection_07', 'Lenço', false, lenco);

    //Init Descriptions
    initDescription();

    var refreshIntervalId = setInterval(() => {
        lengthTotal = dolma.length + calça.length + avental.length + touca.length + sapato.length + bandana.length + lenco.length;
        if($('.display_kit-images .slider-for li').length == lengthTotal){
            $('.display_kit-images').addClass('actived');
            $('.slider-for').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                fade: true,
                asNavFor: '.slider-nav'
              });
              $('.slider-nav').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                asNavFor: '.slider-for',
                dots: false,
                arrows: false,
                centerMode: true,
                focusOnSelect: true
              });
            clearInterval(refreshIntervalId);
        }
    }, 250);

    $('.selection.have_sizes .prod_name').on('change', function (e) {
        var Selection = $(this).attr('id');
        var optionSelected = $("option:selected", this).val();
        loadSizes(Selection, optionSelected);
    });

    $('.selection:not(.have_sizes) .prod_name, .selection .prod_size').on('change', function (e) {
        var selection = $(this).attr('id');
        var optionSelectedPrice = $("option:selected", this).attr('data-price');
        var optionSelectedMax = $("option:selected", this).attr('data-qntMax');
        $(this).parent().parent().find('input[type="number"]').attr('max', optionSelectedMax)
        if(selection == 'selection_01'){
            valorDolma =+ optionSelectedPrice;
        }
        if(selection == 'selection_02'){
            valorCalça =+ optionSelectedPrice;
        }
        if(selection == 'selection_03'){
            valorAvental =+ optionSelectedPrice;
        }
        if(selection == 'selection_04'){
            valorTouca =+ optionSelectedPrice;
        }
        if(selection == 'selection_05'){
            valorSapato =+ optionSelectedPrice;
        }
        if(selection == 'selection_06'){
            valorBandana =+ optionSelectedPrice;
        }
        if(selection == 'selection_07'){
            valorLenco =+ optionSelectedPrice;
        }
        calculatePrice();
    });
    
    $('.qnt_input .plus').click(function (e) { 
        e.preventDefault();
        var oldValue = $(this).parent().find('input').val();
        var checkSize = $(this).parent().parent().parent().attr('class')
        if(checkSize.includes('have_sizes')){
            var dataMax = $(this).parent().parent().parent().find('.prod_size option:selected').attr('data-qntMax');
            if(dataMax != undefined){
                if(oldValue < dataMax){
                    $(this).parent().find('input').val(++oldValue);
                }
            }
        }else{
            var dataMax = $(this).parent().parent().parent().find('.prod_name option:selected').attr('data-qntMax');
            if(dataMax != undefined){
                if(oldValue < dataMax){
                    $(this).parent().find('input').val(++oldValue);
                }
            }
        }
        calculatePrice();
    });

    $('.qnt_input .minus').click(function (e) { 
        e.preventDefault();
        var oldValue = $(this).parent().find('input').val();
        if(oldValue > 0){
            $(this).parent().find('input').val(--oldValue);
        }
        calculatePrice();
    });

    if($('.display_bordado .select_bordado option:selected').val() == 'yes'){
        $('.display_bordado').addClass('actived');
    }
    // if ($('.display_bordado .select_bordado option:selected').val() == 'yes') {
    //     $('.desabilitar_input :input').prop('disabled', true);
    // } else {
    //     $('.desabilitar_input :input').removeProp('disabled');
    // }   
    
    $('.display_bordado .select_bordado').on('change', function (e) {
        var optionSelected = $("option:selected", this).val();
        if(optionSelected == 'yes'){
            $(this).parent().parent().removeClass('desabilitar');
            $(this).parent().parent().addClass('actived');
        }else{
            $(this).parent().parent().removeClass('actived');
            $(this).parent().parent().addClass('desabilitar');
        }
    });

    $('.show_name').html($('.display_bordado input[type="text"]').val());

    $('.display_bordado input[type="text"]').on('input', function (e) {
         value = $(this).val();
        $('.show_name').html(value);
    });
    

    $('.prod_size').click(function (e) { 
        e.preventDefault();
        $(this).parent().parent().removeClass('alert');
    });
    
    $('.select_faculdade, .display_bordado input[type="text"]').click(function (e) { 
        e.preventDefault();
        $(this).removeClass('alert');
    });

    $('.buy_button a').click(function (e) { 
        e.preventDefault();
        var alert = false;
        var faculdade = $('.select_faculdade option:selected').val();
        var não_encontrada = $('#nao_cadastrada:checked').val();
        var bordadoSelect = $('.select_bordado option:selected').val();
        var buttonUrl = $('.buy_button a').attr('href');
        if(não_encontrada == 'on'){
            faculdade = "Faculdade não cadastrada.";
        }else{
            if(faculdade == '' || faculdade == undefined){
                alert = true;
                $('.select_faculdade').addClass('alert');
                Swal.fire({
                    title: 'Opps!',
                    text: 'Parece que temos um erro na seleção do kit',
                    icon: 'error',
                    confirmButtonText: 'Ok'
                })
            }
        }
        $('.selection').each(function (index, element) {
            if($(element).hasClass('have_sizes')){
                var optionName = $(element).find('.prod_name option:selected').val();
                var optionsize = $(element).find('.prod_size option:selected').val();
                if(optionName !== '' && optionName !== undefined){
                    if(optionsize == '' || optionsize == undefined){
                        alert = true;
                        $(element).addClass('alert');
                        Swal.fire({
                            title: 'Opps!',
                            text: 'Parece que temos um erro na seleção do kit',
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        })
                    }
                }
            }
        });
        if(bordadoSelect == 'yes'){
            if($('.display_bordado input[type="text"]').val() == ''){
                alert = true;
                $('.display_bordado input[type="text"]').addClass('alert');
            }
        }
        if(alert == false && buttonUrl !== ''){
            $('.buy_button a').html('Aguarde...')
            $('.buy_button a').css('pointer-events', 'none')
            if(bordadoSelect == 'yes'){
                prodAttach = buttonUrl.substring(buttonUrl.indexOf("add?sku=") + 8);
                prodAttach = prodAttach.split('&qty')[0];
                var attachmentName = 'kit-anexo';
                var content = {
                    Nome: 'Anexo Kit',
                    Aluno: $('.display_bordado input[type="text"]').val(),
                    Faculdade: faculdade
                };
                $.ajax({
                    type: "POST",
                    url: buttonUrl,
                    success: function (response) {
                        Swal.fire({
                            title: 'Parabens!',
                            text: 'Você será redirecionado para o carrinho em instantes.',
                            icon: 'success',
                            confirmButtonText: 'Ok'
                        })
                        vtexjs.checkout.getOrderForm()
                        .done(function (orderForm) {
                            $(orderForm.items).each(function (index, element) {
                                if (prodAttach == orderForm.items[index].id) {
                                    indexItem = index;
                                }
                            });
                            if (indexItem == undefined) {
                                indexItem = 0;
                            }
                            vtexjs.checkout.addItemAttachment(indexItem, attachmentName, content)
                            .done(function () {
                                setTimeout(() => {
                                    window.location.href = "https://www.reidacutelaria.com.br/checkout/";
                                }, 3000);
                            })
                        })
                    }
                });
            }else{
                $.ajax({
                    type: "POST",
                    url: buttonUrl,
                    success: function (response) {
                        Swal.fire({
                            title: 'Parabens!',
                            text: 'Você será redirecionado para o carrinho em instantes.',
                            icon: 'success',
                            confirmButtonText: 'Ok'
                        })
                        setTimeout(() => {
                            window.location.href = "https://www.reidacutelaria.com.br/checkout/";
                        }, 3000);
                    }
                });
            }
        }else{
            Swal.fire({
                title: 'Opps!',
                text: 'Parece que temos um erro na seleção do kit',
                icon: 'error',
                confirmButtonText: 'Ok'
            })
        }
    });
    
});