// WARNING: THE USAGE OF CUSTOM SCRIPTS IS NOT SUPPORTED. VTEX IS NOT LIABLE FOR ANY DAMAGES THIS MAY CAUSE. THIS MAY BREAK YOUR STORE AND STOP SALES. IN CASE OF ERRORS, PLEASE DELETE THE CONTENT OF THIS SCRIPT.
$(window).load(function () {
    //PESQUISA AS FACULDADES
    var header = {
        'Accept': 'application/json',
        'REST-range': 'resources=0-200',
        'Content-Type': 'application/json; charset=utf-8'
    };
    $.ajax({
        url: '/api/dataentities/GN/search?_fields=faculdade&_sort=faculdade ASC',
        type: 'GET',
        headers: header
    }).done(function () {
        console.log("success");
    }).fail(function () {
        console.log("error");
    }).always(function (res) {
        // console.log(res.length);
        var qtb_escolas = res.length;
        for (var c = 0; c < qtb_escolas;) {
            $('#qual-instituicao select').append('<option value="' + res[c].faculdade + '">' + res[c].faculdade + '</option>');
            c++
        }
        $('#qual-instituicao select').append('<option value="outro">Outro</option>'); // Outros em Ultimo item
    });
    //FIM - PESQUISA AS FACULDADES

    //POPUP
    if ($('body').hasClass('body-checkout-confirmation')) {
        //ADD NOME, EMAIL E DATA
        var nomeAluno = $('.pa3.black-80.f6.lh-copy strong').text();
        $('#formulario-check .nomeAluno').val(nomeAluno);
        console.log(nomeAluno);

        var emailAluno = $('.cconf-client-email').text();
        $('#formulario-check .emailAluno').val(emailAluno);
        console.log(emailAluno);

        function dataAtualFormatada() {
            var data = new Date();
            var dia = data.getDate();
            if (dia.toString().length == 1)
                dia = "0" + dia;
            var mes = data.getMonth() + 1;
            if (mes.toString().length == 1)
                mes = "0" + mes;
            var ano = data.getFullYear();

            return dia + "/" + mes + "/" + ano;
        } dataAtualFormatada();

        $('#formulario-check .nData').val(dataAtualFormatada());
        console.log(dataAtualFormatada());

        var numPedido = $('#order-id').text();
        $('#formulario-check .nPedido').val(numPedido);
        console.log(numPedido);
        //FIM - ADD NOME, EMAIL E DATA  

        $('#container-form').addClass('ativo');

        $('.overlay').css('display', 'block');

        $('body').css('overflow-y', 'hidden');

        $('.overlay, .btn-fechar').on('click', function () {
            $('.overlay').css('display', 'none');
            $('#container-form').removeClass('ativo');
            $('#container-form').remove();
            $('body').css('overflow-y', 'scroll');
        });

        $('#alunoGastronomia-nao, #alunoGastronomia').on('click', function () {
            var checked = $(this).parent().attr('for');
            $(".form-step2").removeClass("active");
            $("." + checked).addClass("active");
        });
    }
    //FIM - POPUP

    //POPUP MOBILE
    var width = $(window).width();
    if (width <= 600) {
        $('#container-form').css({ 'top': '55px', 'margin-top': '0px' });
    }
    //FIM - POPUP MOBILE
});


$(window).load(function () {

    var header = {
        'Accept': 'application/json',
        'REST-range': 'resources=0-10',
        'Content-Type': 'application/json; charset=utf-8'
    };

    var insertMasterData = function (ENT, loja, dados, fn) {
        $.ajax({
            url: '/api/dataentities/' + ENT + '/documents/',
            type: 'PATCH',
            data: dados,
            headers: header,
            success: function (res) {
                fn(res);
            },
            error: function (res) {
            }
        });
    };


    // Selecinar OUTRO na opção de faculdade
    $('#qual-instituicao select').change(function () {
        if ($(this).val() == "outro") {
            $('#qual-instituicao').append("<input type='text' id='outro' placeholder='DIGITE O NOME SUA INSTITUIÇÃO' style='max-width: 290px'>")
        } else {
            $('#qual-instituicao #outro').remove();
        }
    });

    //INSERE FORMULÁRIO NO MASTER DATA

    var nome = $('#formulario-check [name="nome"]').val();
    var email = $('#formulario-check [name="email"]').val();
    var nData = $('#formulario-check [name="nData"]').val();
    var nPedido = $('#formulario-check [name="nPedido"]').val();
    $('#formulario-check').submit(function () {
        var alunoGastronomia = $('#formulario-check [name="alunoGastronomia"]:checked').val();
        if (alunoGastronomia == "sim") {
            var instituicao = $('#formulario-check [name="instituicao"]').val();
            var periodo = $('#formulario-check [name="periodo"]:checked').val();
            var semestre = $('#formulario-check [name="semestre"]').val();
            var obj_dados = {
                "nome": nome,
                "email": email,
                "nData": nData,
                "nPedido": nPedido,
                "alunoGastronomia": alunoGastronomia,
                "instituicao": instituicao,
                "periodo": periodo,
                "semestre": semestre,
                "segmento": '-'
            }
        }
        else {
            var segmento = $('#formulario-check [name="segmento"]').val();
            var obj_dados = {
                "nome": nome,
                "email": email,
                "nData": nData,
                "nPedido": nPedido,
                "alunoGastronomia": alunoGastronomia,
                "instituicao": 'Não Informado',
                "periodo": 'Não Informado',
                "semestre": 'Não Informado',
                "segmento": segmento
            }
        }

        var json_dados = JSON.stringify(obj_dados);
        console.log(obj_dados);

        insertMasterData("FC", 'reidacutelaria', json_dados, function (res) {
            console.log(res);
            setTimeout(function () {
                $('#formulario-check').html('<div class="row " style="text-align: center; padding: 60px;"><div class="row"><h2 style="font-size: 2em; font-weight: bold; margin-bottom: 20px;">Cadastro efetuado com sucesso!</h2><p>Em breve você receberá um retorno da nossa equipe de atendimento.</p></div>');
            }, 1000);
        });
    });
    console.log('carregou');
});