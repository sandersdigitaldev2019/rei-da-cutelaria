module.exports = function (grunt) {
  grunt.initConfig({
    // uglify
    uglify: {
      options: {
        mangle: false
      },

      my_target: {
        files: {
          'assets/js/all/all.js': ['assets/js/plugins/plugins.js', 'assets/js/app/app.js'],
        }
      }
    },
    // uglify

    //sass
    sass: {
      dist: {
        files: {
          'assets/css/style.css': 'assets/css/sass/style.scss'
        }
      }
    },
    //sass

    sprite: {
      all: {
        src: 'assets/img/**/*.png',
        dest: 'arquivos/sprite.png',
        destCss: 'assets/css/sprite.css',
        cssTemplate: 'assets/template/handlebarsStr.css.handlebars'
      }
    },

    // concat
    concat: {
      options: {
        separator: '',
      },
      dist: {
        src: ['assets/css/sprite.css', 'assets/css/style.css'],
        dest: 'assets/css/all/all.css',
      },
    },
    // concat


    // css min
    cssmin: {
      combine: {
        files: {
          'arquivos/all.min.css': ['assets/css/all/all.css']
        }
      }
    },
    // css min

    // strip comments
    comments: {
      js: {
        options: {
          singleline: true,
          multiline: true
        },
        src: ['arquivos/all.js']
      },
      css: {
        options: {
          singleline: true,
          multiline: true
        },
        src: ['arquivos/all.min.css']
      }
    },
    // strip comments

    // watch
    watch: {
      dist: {
        files: [
          'assets/js/**/*',
          'assets/css/**/*',
          'assets/img/**/*'
        ],

        tasks: ['sprite', 'sass', 'concat', 'cssmin', 'comments']
      }
    }
    // watch
  });

  // Plugins do Grunt
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-spritesmith');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-stripcomments');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Tarefas que serão executadas
  grunt.registerTask('default', ['sprite', 'sass', 'concat', 'cssmin', 'comments']);

  // Tarefa para Watch
  grunt.registerTask('w', ['watch']);
};