var header = {
    'Accept': 'application/json',
    'REST-range': 'resources=0-300',
    'Content-Type': 'application/json; charset=utf-8'
  };
  
   
  //Formulario - Avise me - Produto Esgotado
  $(document).ready(function(){
      console.log('Fiddler ligado.');
  
      if(($('.notifyme').length)){
          $('.skuList').css('display','block');
          $('.form-avise-me').css('display','block');
          $('.form-avise-me').css('visibility','visible');
      } else {
          $('.form-avise-me').css('visibility','hidden');
          $('.form-avise-me').css('display','none');
      }
      
      $('.skuBestPrice').append('<br>');
      $('.skuPrice').append('<br>');
      
      //$('.preco .kit_final .left .subtotal strong').on('load', function(){  
  //		$('.preco .kit_final .left .subtotal').css('opacity','1');
  //	});
  
      // setTimeout(function(){
      // 	$('.preco .kit_final .left .subtotal strong').css('opacity','1');
      // }, 2800);
  });
  
  $(window).load(function(){
      /* Stuff to do after the page is loaded */
      
      $('.skuBestPrice').append('<br>');
      $('.skuPrice').append('<br>');
  });
  
  
  
  var selectMasterData = function(ENT, params, fn) {
      // Consulta dados na master data
      $.ajax({
          url: '/api/dataentities/' + ENT + '/search?' + params,
          type: 'GET',
          success: function(res) {
              fn(res);
          },
          error: function(res) {
          }
      });
  
  };
  
  
  function somaManos() 
  {
  
      var elems = $('.imagem_kit.kit');
      var arry = $.makeArray(elems)
      var total = 0;
      var link = '';
  
      $.each( arry, function( r, x ){
          var id = $(x).find('#___rc-p-id').attr('value')
          vtexjs.catalog.getProductWithVariations(id).done(function(product){
              var estoque = product.skus[0].available;
              if(estoque == false){
                  $(x).remove()
              }else{
                  alert();
                  link += 'sku='+product.skus[0].sku+'&qty=1&seller=1&';
                  total += product.skus[0].bestPrice << 0;
              }
              $('.buy-button').attr('href', '/checkout/cart/add?'+link);
              var dividido = total/6;
              var dividido = dividido.toFixed(0)
              $('.kit_final .right .subtotal strong').html(dividido);
              $('.kit_final .right .subtotal strong').priceFormat({centsSeparator:",",thousandsSeparator:"."});
              $('.full.preco .subtotal strong').html(total);
              $('.full.preco .subtotal strong').priceFormat({prefix: 'R$ ',centsSeparator:",",thousandsSeparator:"."});
          });
      });
  
      setTimeout(function(){
          $('.preco .kit_final .left .subtotal strong').css('opacity','1');
      }, 2000);
      // $('.preco .kit_final .left .subtotal strong').css('opacity','1');
  }
  
  
  $(window).on('load', function(){
  
      if($('.lancamentos .prateleira-basica li').length <= 0) {
          $('.similar_junto').remove();
      };
  
      $('.thumbs').slick ({
          vertical:true,
          slidesToShow: 5,
            slidesToScroll: 1,
            arrow: true,
            prevArrow: '<button type="button" class="slick-prev">Previous</button>',
            nextArrow: '<button type="button" class="slick-next">Next</button>'
      });
  
  
      $('.prateleira-similar ul').slick ({
          slidesToShow: 2,
            slidesToScroll: 1,
            arrow: true,
            prevArrow: '<button type="button" class="slick-prev">Previous</button>',
            nextArrow: '<button type="button" class="slick-next">Next</button>',
            responsive: [
              {
                breakpoint: 1024,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 3,
                  infinite: true,
                  dots: true
                }
              },
              {
                breakpoint: 600,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2
                }
              },
              {
                breakpoint: 480,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
                }
              }
            ]	
      });
  
  
      $('.carousel-lancamentos .prateleira-basica ul, .carousel-mais-vendidos .prateleira-basica ul').slick ({
          slidesToShow: 4,
            slidesToScroll: 1,
            arrow: true,
            prevArrow: '<button type="button" class="slick-prev">Previous</button>',
            nextArrow: '<button type="button" class="slick-next">Next</button>',
            responsive: [
              {
                breakpoint: 1024,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 3,
                  infinite: true,
                  dots: true
                }
              },
              {
                breakpoint: 600,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2
                }
              },
              {
                breakpoint: 480,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
                }
              }
            ]	
      });
  
  });
  
  $(document).ready(function() {
     $(".prateleira-basica li .title-descricao h1").dotdotdot({
        // Options go here
     });
  });
  
  $(document).ready(function(){
  
      $('body').css('opacity','1');
      
      if($('body.home').length > 0){
          $('.banner').cycle({ 
              fx:     'fade',
              speed:  'slow',
              pause: true,
              timeout: 5000,
              next: '.cycle-next',
              prev: '.cycle-prev',
          });
      };
  
      // Hover Menu Header
      $(".hover_menu").hoverIntent(config);
  
      $('.prateleira-basica > h2').remove();
  
      if($('body.central_atendimento').length > 0){
          $('#accordion').accordion({
              collapsible: true,
              active: false,
              heightStyle: "content"
          });
      };
  
      if($('body.marcas-logos').length > 0){
          $('#tabs').tabs();
      };
  
      $('.shipping-value').trigger('click');
  
      // $('.titulo-parcelamento').addClass('sprite-parcelamento');
  
      // Menu flutuante e hamburger
    
      $(window).on('scroll', function () {
  
          var headerHeight = $('header').outerHeight();
  
          if ($(window).scrollTop() > headerHeight) {
              $('header.desktop').addClass('fixed');
              $('.logotipo').addClass('logotipo-mobile');
              $('.logotipo-mobile').removeClass('logotipo');
          } else {
              $('header.desktop').removeClass('fixed');
              $('.logotipo-mobile').addClass('logotipo');
              $('.logotipo').removeClass('logotipo-mobile');
          }
      });
  
      $('.open-nav-mobile').on('click', function(e){
          e.preventDefault();
          $(this).toggleClass('active');
          $('.menu').toggleClass('active');
          $('.contact-menu').removeClass('active');
          $('.open-contact-mobile a').parent().removeClass('active');
          $('.open-contact-mobile a').find('.sprite-headphone-active').addClass('sprite-headphone');
          $('.open-contact-mobile a').find('.sprite-headphone').removeClass('sprite-headphone-active');
          $('header.fixed').css('height','auto');
      });
  
      $('.open-contact-mobile a').on('click', function(e){
          e.preventDefault();
          $(this).parent().toggleClass('active');
          $('.contact-menu').toggleClass('active');
          $('.menu').removeClass('active');
          $('.open-nav-mobile').removeClass('active');
  
          if ($(this).parent().hasClass('active')) {
              $(this).find('.sprite-headphone').addClass('sprite-headphone-active');
              $(this).find('.sprite-headphone-active').removeClass('sprite-headphone');
              $('header.fixed').css('height','75px');
          } else {
              $(this).find('.sprite-headphone-active').addClass('sprite-headphone');
              $(this).find('.sprite-headphone').removeClass('sprite-headphone-active');
              $('header.fixed').css('height','auto');
          }
      });
  
      if($('.async').length > 0){
          $('li.helperComplement').remove();
          if($('.comprar-produto .skus.full').length > 0){
              
          }else{
              var elems = $('.async li');
              var arr = $.makeArray(elems)
              $.each( arr, function( r, x ){
                  var id = $(x).find('.produto').data('id');
                  // console.log(id);
                  // $(x).find('.comprar-produto').append('<div class="skus full"><p>Selecione a opÃƒÆ’Ã‚Â§ÃƒÆ’Ã‚Â£o:</p>');
                  // $(x).find('.comprar-produto').append('<div class="escolhas full"><p>Quantidade:');
                  setTimeout(function(){
                      vtexjs.catalog.getProductWithVariations(id).done(function(product){
                          var skus_qtd = product.skus.length;
                          // console.log(skus_qtd);
  
                          if(skus_qtd> 3){
                              $(x).find('.comprar-produto .skus.full').append('<select class="escolha_sku ativo">')
                              for (var c = 0; c < skus_qtd; c++) {
                                  $(x).find('.comprar-produto .skus.full select.ativo').append('<option data-sku="'+product.skus[c].sku+'" data-seller="'+product.skus[c].sellerId+'" val="'+product.skus[c].skuname+'">'+product.skus[c].skuname+'');
                              }
                              $('.comprar-produto .skus.full select').change(function(event) {
                                  var sku_select = $(this).find('option:selected').attr('data-sku');
                                  var seller_select = $(this).find('option:selected').attr('data-seller');
                                  $(this).attr('data-sku', sku_select);
                                  $(this).attr('data-seller', seller_select);
                              });
                          }else{
                              // console.log('ddddd');
                              for (var c = 0; c < skus_qtd; c++) {
                                  if(c == 0){
                                      // console.log('ddddd');
                                      $(x).find('.botoes').append('<div class="sku ativo" data-id="'+product.productId+'" data-sku="'+product.skus[c].sku+'" data-seller="'+product.skus[c].sellerId+'">'+product.skus[c].skuname+'');
                                  }else{
                                      $(x).find('.botoes').append('<div class="sku" data-id="'+product.productId+'"  data-sku="'+product.skus[c].sku+'" data-seller="'+product.skus[c].sellerId+'">'+product.skus[c].skuname+'');
                                  }
                              }
                          }
                          // $(x).find('.comprar-produto .escolhas.full').append('<div class="quantidade"><input type="text" value="1" /><div class="sprite-mais" onclick="adicionar(event)"></div><div class="sprite-menos" onclick="tirar(event)"></div>');
                          // $(x).find('.comprar-produto .escolhas.full').append('<a href="javascript:void(0)" class="sprite-comprar-async" onclick="addCart(event)">');
                      });
                  },500);
              });
          }
      }
  
      $('.sprite-open-cart').click(function(event) {
          $('#carrinhoFloat').css('right', '0');
          setTimeout(function(){
              $('#carrinhoFloat').removeAttr('style');
          },3500);
      });
  
      if($('body.marcas-sanders').length > 0){
          function buscaMarcas() {
              $('#tabs ul li').on('click', function(){
                var ordem = $(this).text();
                console.log('cliquei ' + ordem);
                $('#txtBusca').val(ordem);
                
                
                var texto = $('#txtBusca').val();
                console.log(texto);
  
                    if ($('#txtBusca').val() === 'A-Z') {
                        $(".array-marcas a").css("display", "block");
                    } else {
                      $(".array-marcas a").css("display", "block");
                      $(".array-marcas a").each(function(){
                            if($(this).attr('href').substring(0, 1).indexOf(texto.toUpperCase()) < 0)
                          $(this).css("display", "none");
                        });
                    };
                });
          } buscaMarcas();
  
          header = {
              "accept": "application/json",
              "rest-range": "resources=0-300",
              'Content-Type': 'application/json; charset=utf-8'
          }
          $.ajax({
              url: '/api/dataentities/MC/search?_fields=nome,link,imagem,id',
              type: 'GET',
              headers: header
          }).
          done(function() {
              console.log("success");
          }).fail(function() {
              console.log("error");
          }).always(function(res) {
              console.log(res);
              var qtd_marcas = res.length;
  
              for (var c = 0; c < qtd_marcas;) {
  
  
                  $('.array-marcas').append('<a href="'+res[c].nome+'" target="_blank"><img src="http://api.vtex.com/reidacutelaria/dataentities/MC/documents/'+res[c].id+'/imagem/attachments/'+res[c].imagem+'" /></a>');
                  c++
              }
          });
      };
  
  
      if($('body.gastronomia').length > 0){
          header = {
              "accept": "application/json",
              "rest-range": "resources=0-300",
              'Content-Type': 'application/json; charset=utf-8'
          }
          $.ajax({
              url: '/api/dataentities/GN/search?_fields=faculdade,link,logo,id&_sort=faculdade ASC',
              type: 'GET',
              headers: header
          }).
          done(function() {
              console.log("success");
          }).fail(function() {
              console.log("error");
          }).always(function(res) {
              console.log(res);
              var qtb_escolas = res.length;
              var maiorPreco = "&O=OrderByPriceDESC";
  
              for (var c = 0; c < qtb_escolas;) {
                  $('#carrinhoFloat').removeAttr('style');
                  $('.faculdades').append('<a href=/'+res[c].link+maiorPreco+'><img src="http://api.vtex.com/reidacutelaria/dataentities/GN/documents/'+res[c].id+'/logo/attachments/'+res[c].logo+'" <span><em style="font-size: 15px;height: 40px; float: left; width: 100%;">'+res[c].faculdade+'</em></span></a>');
                  c++
              }
          });
      };
  
      if($('body.departamento').length > 0){
          header = {
              "accept": "application/json",
              "rest-range": "resources=0-300",
              'Content-Type': 'application/json; charset=utf-8'
          }
          $.ajax({
              url: '/api/dataentities/GN/search?_fields=faculdade,link',
              type: 'GET',
              headers: header
          }).
          done(function() {
              console.log("success");
          }).fail(function() {
              console.log("error");
          }).always(function(res) {
              console.log(res);
              var qtb_escolas = res.length;
              var maiorPreco = "&O=OrderByPriceDESC";
  
              for (var c = 0; c < qtb_escolas;) {
                  $('#carrinhoFloat').removeAttr('style');
                  $('aside .lista_faculdades > div').append('<a href=/'+res[c].link+maiorPreco+'><span><em>'+res[c].faculdade+'</em></span></a>');
                  c++
              }
  
              setTimeout(function(){
  
                  function NASort(a, b) {    
                      return (a.innerHTML > b.innerHTML) ? 1 : -1;
                  };
  
                  $('aside .lista_faculdades > div a').sort(NASort).appendTo('aside .lista_faculdades > div');
              },500);
          });
      };
  });
  
  // CAROUSEL
  $(window).load(function() {
          if($('body.produto-interna').length > 0){
  
          $('.box_compre_junto div div ul').cycle({ 
              fx:     'scrollHorz',
              speed:  'fast',
              timeout: 0,
              next:   '#next-compre',
              prev:   '#prev-compre',
              before: function(){
                  $(this).parent().find('li.ativo').removeClass();
              },
              after: function(){
                  $(this).addClass('ativo');
                  var ativo = $('.box_compre_junto div div ul li.ativo div.sku-data').data('id');
                  vtexjs.catalog.getProductWithVariations(ativo).done(function(product){
  
                      $('.box_compre_junto div div ul li.ativo div.sku-data').attr('data-preco' , product.skus[0].bestPrice);
  
                      var preco_prod_pag = $('#produto-fixo').data('preco');
                      console.log('produto da esquerda: ' + preco_prod_pag);
  
                      var preco_prod_ativo = $('.box_compre_junto div div ul li.ativo div.sku-data').data('preco');
                      console.log('produto da direita: ' + preco_prod_ativo);
  
  
                      var valor_compre = parseInt(preco_prod_ativo) + parseInt(preco_prod_pag);
                      var comprar_prod = $('#produto-fixo').data('compra');
                      var comprar_junto = $('.box_compre_junto div div ul li.ativo div.sku-data').data('compra');
                      $('.total_junto em').text(valor_compre);
                      $('.btn_comprar_tudo').attr('href','/checkout/cart/add?'+comprar_prod+comprar_junto+'');
  
                      $('.total_junto em').priceFormat({
                          prefix: 'R$ ',
                          centsSeparator: ',',
                          thousandsSeparator: '.'
                      });
                  });
              }
          });
  
          // $('.similar_junto').remove();
  
          var video = $('.value-field.url-Video').text();
          console.log(video);
          if(video != ''){
              console.log('show');
              $('.thumbs').prepend('<li class="thumb-video"><div><img style="" src="/arquivos/thumb_video.png" /></div></li>');
              $('#image').append('<div class="iframe-youtube"><iframe width="100%" height="315" src="https://www.youtube.com/embed/'+video+'" frameborder="0" allowfullscreen></iframe></div>');
              $('.thumb-video img').click(function(event) {
                  $('.iframe-youtube').css('z-index','1');
              });
              $('.thumbs a').click(function(event) {
                  $('.iframe-youtube').css('z-index','-1');
              });
          };
  
          // var video = $('.value-field.url-Video').length;
          
          // var urlvideo = $('.value-field.url-Video > iframe').attr('src');
          // console.log(urlvideo);
          // if(video > 0){
          // 	console.log('show');
          // 	$('.thumbs').prepend('<li class="thumb-video"><div><img style="" src="/arquivos/thumb_video.png" /></div></li>');
          // 	$('body.produto-interna').append('<div class="alpha-video"></div><div class="iframe-video-prod"><div class="close-video">X</div></div>');
          // 	$('.thumb-video').click(function(event) {
          // 		$(".alpha-video").show();
          // 		$('.iframe-video-prod').show().append('<iframe allowfullscreen="" webkitallowfullscreen="" mozallowfullscreen="" width="100%" height="315" src="'+urlvideo+'" scrolling="no" frameborder="0" allowtransparency="true"></iframe>');
          // 	});
          // 	$('.thumbs a,.alpha-video,.close-video').click(function(event) {
          // 		$(".alpha-video").hide();
          // 		$('.iframe-video-prod iframe').remove();
          // 		$('.iframe-video-prod').hide();
          // 	});
          // };
      };
  
  
      if($('body.gastronomia').length > 0){
          $(".gastronomia form input").keyup(function(){
              var texto = $(this).val();
              
              $(".faculdades a").css("display", "table");
              $(".faculdades a").each(function(){
                  if($(this).text().toUpperCase().indexOf(texto.toUpperCase()) < 0)
                      $(this).css("display", "none");
              });
          });
      };
  
      $('.helperComplement').remove();
      var elems = $('.box_compre_junto ul li');
      var arr = $.makeArray(elems);
      $.each( arr, function( r, x ){
          var id = $(x).find('.sku-data').attr('data-id');
          vtexjs.catalog.getProductWithVariations(id).done(function(product){
              $(x).find('.sku-data').attr('data-compra','&sku='+product.skus[0].sku+'&qty=1&seller='+product.skus[0].sellerId+'');
              $(x).find('.sku-data').attr('data-preco', product.skus[0].bestPrice);
          });
          $(x).find('.sku-data').addClass('data-posicao-'+r+'');
      });
  
  
      // KIT LOOK - INÃƒÆ’Ã‚ÂCIO
      if($('body.produto-interna.produto-look').length > 0){
          var elems = $('.imagem_kit.kit');
          var arr = $.makeArray(elems)
          $.each( arr, function( r, x ){
              if($(x).find('.apresentacao').length > 0){        		
                  $('[class*=skuespec]').click(function(event) {
                      if($(this).parents('.full.kit').find('.select_kit').hasClass('ativo')){
                          var $obj = $(this);
                          setTimeout(function(){
                              var numeral = $obj.parents('.full.kit').find('.skuPrice').text();
                              $obj.parents('.full.kit').attr('data-valor', apenasNumeros(numeral));
                          }, 300);
                      }
                  });
                  $('.select_kit').click(function(){
                      if($(this).hasClass('ativo')){
                          $(this).removeClass('ativo');
                          $(this).parent('.full.kit').removeClass('ativo');
                          $(this).parent('.full.kit').removeAttr('data-valor');
                      }else{
                          var numeral = $(this).parent('.full.kit').find('.skuPrice').text()
                          $(this).parent('.full.kit').attr('data-valor', apenasNumeros(numeral))
                          $(this).addClass('ativo');
                          $(this).parent('.full.kit').addClass('ativo');
                          $(this).parent('.full.kit').find('[class*=skuespec]:eq(0)').trigger('click')
                      }
                  });
                  $('.select_kit.ativo').click(function(){
                      $(this).removeClass('ativo');
                  });
                  
                  if($(x).find('.apresentacao').length > 3){
                      $('.segura_kits').mCustomScrollbar({
                          scrollButtons:{enable:true},
                          theme:"dark-thin",
                          scrollbarPosition:"outside"
                      });
                  }
              }else{
                  $(x).remove()
              }
          });
  
  
          var video = $('.value-field.ID-do-youtube').text();
          console.log(video)
          if(video != ''){
              console.log('show');
              $('.thumbs').append('<li class="thumb-video"><div><img style="" src="http://img.youtube.com/vi/'+video+'/default.jpg" /></div></li>');
              $('#image').append('<div class="iframe-youtube"><iframe width="100%" height="315" src="https://www.youtube.com/embed/'+video+'" frameborder="0" allowfullscreen></iframe></div>');
              $('.thumb-video').click(function(event) {
                  $('.iframe-youtube').css('z-index','1');
              });
              $('.thumbs a').click(function(event) {
                  $('.iframe-youtube').css('z-index','-1');
              });
          };
          
      };
      // KIT LOOK - FIM
      
  
      if($('body.home').length > 0){
          // MARCA
          $('#logoMarcas .jcarousel').jcarousel();
          // MARCA
          
          // PRODUTO
          $('.box_compre_junto h2').remove();
          $('.jcarousel div div').jcarousel();
          // PRODUTO
  
          // SETAS
          $('.jcarousel-control-prev')
              .on('jcarouselcontrol:active', function() {
                  $(this).removeClass('inactive');
              })
              .on('jcarouselcontrol:inactive', function() {
                  $(this).addClass('inactive');
              })
              .jcarouselControl({
                  target: '-=1'
              });
  
          $('.jcarousel-control-next')
              .on('jcarouselcontrol:active', function() {
                  $(this).removeClass('inactive');
              })
              .on('jcarouselcontrol:inactive', function() {
                  $(this).addClass('inactive');
              })
              .jcarouselControl({
                  target: '+=1'
              });
          // SETAS
      };
  
      if($('body.produto-interna.produto-look').length > 0){
          $('.jcarousel .prateleira-basica h2').remove();
          // PRODUTO
          $('.jcarousel div > div').jcarousel();
  
          $('.jcarousel div > div')
              .on('jcarousel:targetin', 'li', function() {
                  $(this).addClass('target');
              })
              .on('jcarousel:targetout', 'li', function() {
                  $(this).removeClass('target');
              });
          // PRODUTO
  
  
          // SETAS
          $('.jcarousel-control-prev')
              .on('jcarouselcontrol:active', function() {
                  $(this).removeClass('inactive');
              })
              .on('jcarouselcontrol:inactive', function() {
                  $(this).addClass('inactive');
              })
              .jcarouselControl({
                  target: '-=1'
              });
  
          $('.jcarousel-control-next')
              .on('jcarouselcontrol:active', function() {
                  $(this).removeClass('inactive');
              })
              .on('jcarouselcontrol:inactive', function() {
                  $(this).addClass('inactive');
              })
              .jcarouselControl({
                  target: '+=1'
              });
          // SETAS
      };
  
      // COMPRE JUNTO
      if($('body.produto, body.produto-interna.normal').length > 0){
          vtexjs.catalog.getCurrentProductWithVariations().done(function(product){
              // console.log(product.skus[0].image);
              // console.log(product.name);
              // console.log('ou ÃƒÆ’  vista de '+product.skus[0].bestPriceFormated);
  
              var img_prod = product.skus[0].image;
              var preco = product.skus[0].bestPrice;
              console.log('var preco = ' +preco);
              var nome_prod = product.name;
              var sku_prod = product.skus[0].sku;
              var preco_prod = 'Por: '+product.skus[0].bestPriceFormated;
              console.log(preco_prod);
              var data_compra = '&sku='+product.skus[0].sku+'&qty=1&seller='+product.skus[0].sellerId+'';
              var url_prod = 'http://reidacutelaria.vtexcommercestable.com.br/checkout/cart/add?sku='+ sku_prod +'&qty=1&seller=1&redirect=true&sc=1';
              // console.log(url_prod);
  
              // $('#img_prod').html(img_prod);
              $('#produto-fixo').attr('data-preco', preco);
              $('#produto-fixo').attr('data-compra', data_compra);
              $('#img_prod img').attr('src', img_prod);
              $('#img_prod img').attr('alt', nome_prod);
              $('#nome_prod').html(nome_prod);
              $('#preco_prod').html(preco_prod);
              $('#url_prod').attr('href', url_prod);
          });
  
  
          $('.jcarousel .prateleira-basica h2').remove();
          // PRODUTO
          $('.jcarousel div > div').jcarousel();
  
          $('.jcarousel div > div')
              .on('jcarousel:targetin', 'li', function() {
                  $(this).addClass('target');
              })
              .on('jcarousel:targetout', 'li', function() {
                  $(this).removeClass('target');
              });
          // PRODUTO
  
  
          // SETAS
          $('.jcarousel-control-prev')
              .on('jcarouselcontrol:active', function() {
                  $(this).removeClass('inactive');
              })
              .on('jcarouselcontrol:inactive', function() {
                  $(this).addClass('inactive');
              })
              .jcarouselControl({
                  target: '-=1'
              });
  
          $('.jcarousel-control-next')
              .on('jcarouselcontrol:active', function() {
                  $(this).removeClass('inactive');
              })
              .on('jcarouselcontrol:inactive', function() {
                  $(this).addClass('inactive');
              })
              .jcarouselControl({
                  target: '+=1'
              });
          // SETAS
      };
      // COMPRE JUNTO
  
  
      if($('body.departamento.resultado-busca').length > 0){
  
          var lista_marcas = 0;
          $('aside .lista_marcas > span, aside .lista_faculdades > span').click(function(){
              if(lista_marcas == 1){
                  $('aside .lista_marcas > div, aside .lista_faculdades > div').removeClass('ativo');
                  lista_marcas--;
              }else{
                  $('aside .lista_marcas > div, aside .lista_faculdades > div').addClass('ativo');
                  lista_marcas++;
              }
          });
  
          var url = window.location.pathname.split('/')[1];
          var urlF = window.location.search;
          var urlF = urlF.indexOf("?fq=H:");
          console.log(urlF);
          if (urlF > -1) {
              $('.lista_faculdades').css('display', 'block');
          };
  
          if (url == "ANL-Brasil") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_ANL_Brasil.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Aramfactor") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Aramfactor.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Artinox") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Artinox.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Batil") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Batil.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Bertoldi") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Bertoldi.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Bianchi") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Bianchi.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Bic") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Bic.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Bohnenberger") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Bohnenberger.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Boker") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Boker.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Brinox") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Brinox.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Caparroz") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Caparroz.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Carborundum") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Carborundum.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Casa-Arabe") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Casa_Arabe.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Cimo") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Cimo.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Cold-Steel") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Cold-Steel.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Corneta") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Corneta.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Costa-e-Fio") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Costa_e_Fio.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "CRKT") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_CRKT.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "D-Addario") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_D_Addario.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Deval") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Deval.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Di-Solle") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Di_Solle.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Dinox") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Dinox.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Doupan") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Doupan.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Dovo") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Dovo.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Eltoro") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Eltoro.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Euclides-Jordao") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Euclides_Jordao.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Fenix") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Fenix.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Fidalga") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Fidalga.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Gerber") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Gerber.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Giannini") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Giannini.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Global") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Global.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Hercules") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Hercules.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Hering") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Hering.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Iberia") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Iberia.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Imperial-Schrade") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Imperial-Schrade.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Importado") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Importado.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Inborplas") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Inborplas.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Incoterm") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Incoterm.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Izzo") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Izzo.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Keita") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Keita.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Kershaw") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Kershaw.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Kyocera") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Kyocera.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Lamare") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Lamare.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Lan") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Lan.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Leatherman") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Leatherman.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "LM-Paschoalini") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_LM_Paschoalini.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Lume") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Lume.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Luminox") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Luminox.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Mago") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Mago.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Mantis-Knives") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Mantis-Knives.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Marcato") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Marcato.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Master-Cutlery") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Master-Cutlery.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Merheje") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Merheje.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Metalcan") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Metalcan.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Metaltex") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Metaltex.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Mtech") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Mtech.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Mundial") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Mundial.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Nautika") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Nautika.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Olfa") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Olfa.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Pado") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Pado.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Pazini") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Pazini.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Polyplast") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Polyplast.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Professional-Cheff") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Professional_Cheff.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Pronyl") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Pronyl.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Rapozo") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Rapozo.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Ricsen") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Ricsen.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Rouxinol") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Rouxinol.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Sander-Irmaos") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Sander_Irmaos.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Sao-Benedito") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Sao_Benedito.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Shun") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Shun.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Sica") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Sica.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Smith & Wesson") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Smiths") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Smiths.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Soft-Case") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Soft_Case.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "South-American") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_South_American.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Spagnol") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Spagnol.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Super-Barba") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Super_Barba.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Sutaco") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Sutaco.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Swiza") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Swiza.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Taumer") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Taumer.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Torelli") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Torelli.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Tramontina") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Tramontina.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Treis-Reis") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Treis_Reis.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Trim") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Trim.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "USMC") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_USMC.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "United-Cutlery") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_United-Cutlery.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Viel") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Viel.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Volcano") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Volcano.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Western") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Western.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Zippo") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Zippo.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
          if (url == "Zwilling") {
              $('aside').prepend('<div class="full dept_marcas"><img src="/arquivos/logo_Zwilling.jpg" alt="" /></div>');
              $('.lista_marcas').css('display', 'block');
          };
      };
  
  });
  
  function addCart(event){
      var parente = $(event.target).parents('li')
      var skus_click = parente.find('.ativo').attr('data-sku');
      var seller_click = parente.find('.ativo').attr('data-seller');
      console.log(seller_click);
      item = {
          id: skus_click,
          quantity: 1,
          seller: seller_click
      };
      vtexjs.checkout.addToCart([item]).done(function(orderForm){
          console.log([item]);
          console.log(orderForm);
      });
      parente.addClass('adicionado');
      $('#carrinhoFloat').css('right', '0');
      setTimeout(function(){
          parente.removeClass('adicionado')
          $('#carrinhoFloat').removeAttr('style')
      },3500);
  }
  
  
  // MOBILE
  $(window).load(function(){
      somaManos()
  
      var width = $(window).width();
      var height = $(window).height();
      var height_img = $('.slider .box-banner a img').height();
      var menu_button = 65;
      var menu_width = (width) - (menu_button);
      var menu_topo = 0;
      var filtro = 0;
      var seta_desc = 0;
      
      //$('.slider').css('height', height_img );
  
      // console.log(menu_width);
      $('.sprite-ico_menu_mobile').click(function(){
          if(menu_topo == 1){
              $('.menu').removeClass('ativo');
              $('.sprite-ico_menu_mobile').removeClass('ativo');
              $('body').css('position', 'relative');
              $('.menu ul').css('width', "0" );
              menu_topo--;
          }else{
              $('.menu').addClass('ativo');
              $('.sprite-ico_menu_mobile').addClass('ativo');
              $('body').css('position', 'fixed');
              $('.menu ul').css('width', menu_width );
              menu_topo++;
          }
      });
      $('.fechar_menu').click(function(event) {
          $('.menu').removeClass('ativo');
          $('.sprite-ico_menu_mobile').removeClass('ativo');
          $('body').css('position', 'relative');
          $('.menu ul').css('width', "0" );
          menu_topo--;
      });
  
  
  
      // FILTRO MOBILE - DEPARTAMENTO
      if($('body.departamento').length > 0){
          $('.side').css('height', height-156);
          $('.abre-filtro').click(function(){
              if(filtro == 1){
                  $('.side').css('display', 'none');
                  $('body').css('position', 'relative');
                  filtro--;
              }else{
                  $('.side').css('display', 'block');
                  $('body').css('position', 'fixed');
                  filtro++;
              }
          });
      };
  
      // FILTRO MOBILE - INSTITUCIONAL
      if($('.institucional').length > 0){
          $('.institucional aside.left').css('height', height-156);
          $('.abre-filtro').click(function(){
              if(filtro == 1){
                  $('.institucional aside.left').css('display', 'none');
                  $('body').css('position', 'relative');
                  filtro--;
              }else{
                  $('.institucional aside.left').css('display', 'block');
                  $('body').css('position', 'fixed');
                  filtro++;
              }
          });
      };
  
  
      // DESCRIÃƒÆ’Ã¢â‚¬Â¡ÃƒÆ’Ã†â€™O E AVALIAÃƒÆ’Ã¢â‚¬Â¡ÃƒÆ’Ã†â€™O PRODUTO
      setTimeout(function(){
          var height_descricao = $('.descricoes .full').height();
          var height_avaliacao = $('.box-avaliacao').height();
          //console.log(height_descricao);
          $('.mobile .descricoes .full').css('height', '0');
          $('.mobile .box-avaliacao .full').css('height', '0');
          if($('.produto').length > 0){
              $('.mobile.produto .descricoes h2').click(function(){
                  if(seta_desc == 1){
                      $('.mobile.produto .descricoes h2').removeClass('ativo');
                      $('.mobile .descricoes .full').css('height', '0');
                      seta_desc--;
                  }else{
                      $('.mobile.produto .descricoes h2').addClass('ativo');
                      $('.mobile .descricoes .full').css('height', height_descricao);
                      seta_desc++;
                  }
              });
  
              $('.mobile .box-avaliacao h2').click(function(){
                  if(seta_desc == 1){
                      $('.mobile.produto .box-avaliacao h2').removeClass('ativo');
                      $('.mobile .box-avaliacao .full').css('height', '0');
                      seta_desc--;
                  }else{
                      $('.mobile.produto .box-avaliacao h2').addClass('ativo');
                      $('.mobile .box-avaliacao .full').css('height', height_avaliacao);
                      seta_desc++;
                  }
              });
          };	
      }, 3000);
  
  
  });
  
  $(window).load(function(){
      // $('.titulo-parcelamento').click(function() {
      // 	$('.other-payment-method-ul').css('display', 'block');
      // });
  
      var abre_parcelas = 0;
      // console.log('AQUIIIIIIIIII');
  
      setTimeout(function(){
          $('.titulo-parcelamento').click(function(){
              if(abre_parcelas == 1){
                  $('.titulo-parcelamento').removeClass('ativo');
                  $('.other-payment-method-ul').css('display', 'none');
                  abre_parcelas--;
                  // console.log('entrou');
              }else{
                  $('.titulo-parcelamento').addClass('ativo');
                  $('.other-payment-method-ul').css('display', 'block');
                  abre_parcelas++;
                  // console.log('saiu');
              }
          });
      }, 3000);
  
      $('.produto-look').css('display', 'block');
  
  
      // TROCA IMAGEM SKU
      $('.top_kit > .right').html('<img src="" />');
      $('.imagem_kit.kit').click(function(){
        var imagem = $(this).find('#image img').attr('src').split('/')
        var imagem_alta = $(this).find('#image img').attr('src').split('/')[5].split('-');
        // console.log(imagem);
        // console.log(imagem_alta);
        // console.log($(this).attr('src').split('/')[5].split('-')[1])
        // console.log($(this).attr('src').split('/')[5].split('-')[2])
        $('.top_kit > .right img').attr('src', imagem[0]+'//'+imagem[1]+'/'+imagem[2]+'/'+imagem[3]+'/'+imagem[4]+'/'+imagem_alta[0]+'-370-370'+'/'+imagem[6]);
        // $('#botaoZoom img').attr('src', imagem[0]+'//'+imagem[1]+'/'+imagem[2]+'/'+imagem[3]+'/'+imagem[4]+'/'+imagem_alta[0]+'-111-111/'+imagem[6]);
      });
      $('.imagem_kit.kit:eq(0)').trigger('click');
  
      // TROCA IMAGEM SKU
  });
  
  
  // FUNÃƒÆ’Ã¢â‚¬Â¡ÃƒÆ’Ã†â€™O PARA O HOVER DO MENU PRINCIPAL
  function megaHoverOver(){
      $(this).find(".menu_hover").stop().fadeTo('fast', 1).show();
      $(".menu_hover").css({'z-index':'999999'});
  }
  function megaHoverOut(){ 
   $(this).find(".menu_hover").stop().fadeTo('fast', 0, function() {
      }).hide();
  }
  var config = {    
       sensitivity: 12, // number = sensitivity threshold (must be 1 or higher)    
       interval: 10, // number = milliseconds for onMouseOver polling interval    
       over: megaHoverOver, // function = onMouseOver callback (REQUIRED)    
       timeout: 150, // number = milliseconds delay before onMouseOut    
       out: megaHoverOut // function = onMouseOut callback (REQUIRED)    
  };
  
  // MOBILE
  $(document).ready(function() {
  
    
    $(window).resize(function() {
          var width = $(window).width();
          var height = $(window).height();
          var height_img = $('.banner_mb .box-banner a img:visible').height();
  
          $('.banner_mb').css('height', height_img);
     });
  
      $('.banner-mobile .center .box-1').slick ({
          slidesToShow: 1,
            slidesToScroll: 1,
            arrow: true,
            dots:false,
            autoplay:true,
            prevArrow: '<button type="button" class="slick-prev">Previous</button>',
            nextArrow: '<button type="button" class="slick-next">Next</button>',
            responsive: [
              {
                breakpoint: 1024,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 3,
                  infinite: true,
                  dots: false
                }
              },
              {
                breakpoint: 600,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2
                }
              },
              {
                breakpoint: 480,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
                }
              }
            ]	
      });
              
  
    var check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
    console.log(check);
  
      if(check == true)
      {
         // console.log('mobile')
          
          if($('body.home').length > 0){
              $('.banner_mb .ajusta_banner').cycle({ 
              fx:     'fade',
              speed:  'slow',
              pause: 	 true,
              timeout: 6000,
              next:   '#next', 
              prev:   '#prev'
          });
          }
  
          var width = $(window).width();
          var height = $(window).height();
          var height_img = $('.banner_mb .box-banner a img:visible').height();
          var menu_button = 50;
          var menu_width = (width) - (menu_button);
          var menu_topo = 0;
          var filtro = 0;
          var seta_desc = 0;
          
          //$('.slider').css('height', height_img );
          $('.banner_mb').css('height', height_img);
  
          // $('.menu_mobile').css('width', width-54);
          // console.log(menu_width);
          // MENU MOBILE
          $('.sprite-menu_mobile').click(function(){
              if(menu_topo == 1){
                  // $('.menu_mobile').removeClass('ativo');
                  $('.menu_mobile').css('width', 0);
                  $('.sprite-menu_mobile').removeClass('ativo');
                  $('.fundo_black').remove();
                  $('body').css('position', 'relative');
                  menu_topo--;
              }else{
                  // $('.menu_mobile').addClass('ativo');
                  $('.menu_mobile').css('width', width-50);
                  $('.sprite-menu_mobile').addClass('ativo');
                  $('header.mobile').prepend('<div class="fundo_black"></div>');
                  $('body').css('position', 'fixed');
                  menu_topo++;
              }
          });
          // MENU MOBILE
  
          // FILTRO MOBILE - DEPARTAMENTO
          if($('.departamento aside').length > 0){
              $('aside').css('height', height-101);
              $('.abre-filtro').click(function(){
                  if(filtro == 1){
                      $('aside').css('display', 'none');
                      $('body').css('position', 'relative');
                      filtro--;
                  }else{
                      $('aside').css('display', 'block');
                      $('body').css('position', 'fixed');
                      filtro++;
                  }
              });
          };
  
          // FILTRO MOBILE - INSTITUCIONAL
          if($('.central_atendimento aside').length > 0){
              $('aside').css('height', height-101);
              $('.abre-filtro').click(function(){
                  if(filtro == 1){
                      $('aside').css('display', 'none');
                      $('body').css('position', 'relative');
                      filtro--;
                  }else{
                      $('aside').css('display', 'block');
                      $('body').css('position', 'fixed');
                      filtro++;
                  }
              });
          };
  
  
          $(window).resize(function() {
              console.log('oiiiiiiiiii');
  
              if($('body.home').length > 0){
                  $('.banner_mb .ajusta_banner').cycle({ 
                  fx:     'fade',
                  speed:  'slow',
                  pause: 	 true,
                  timeout: 6000,
                  next:   '#next', 
                  prev:   '#prev'
              });
              }
  
              var width = $(window).width();
              var height = $(window).height();
              var height_img = $('.banner_mb .box-banner a img:visible').height();
              var menu_button = 50;
              var menu_width = (width) - (menu_button);
              var menu_topo = 0;
              var filtro = 0;
              var seta_desc = 0;
              
              //$('.slider').css('height', height_img );
              $('.banner_mb').css('height', height_img);
  
              // var tamanho_menu = $('.menu_mobile').css('width', width-54);
              // console.log(menu_width);
              // MENU MOBILE
              $('.sprite-menu_mobile').click(function(){
                  if(menu_topo == 1){
                      // $('.menu_mobile').removeClass('ativo');
                      $('.menu_mobile').css('width', 0);
                      $('.sprite-menu_mobile').removeClass('ativo');
                      $('.fundo_black').remove();
                      $('body').css('position', 'relative');
                      menu_topo--;
                  }else{
                      // $('.menu_mobile').addClass('ativo');
                      $('.menu_mobile').css('width', width-50);
                      $('.sprite-menu_mobile').addClass('ativo');
                      $('header.mobile').prepend('<div class="fundo_black"></div>');
                      $('body').css('position', 'fixed');
                      menu_topo++;
                  }
              });
              // MENU MOBILE
  
  
              // FILTRO MOBILE - DEPARTAMENTO
              if($('.departamento aside').length > 0){
                  $('aside').css('height', height-101);
                  $('.abre-filtro').click(function(){
                      if(filtro == 1){
                          $('aside').css('display', 'none');
                          $('body').css('position', 'relative');
                          filtro--;
                      }else{
                          $('aside').css('display', 'block');
                          $('body').css('position', 'fixed');
                          filtro++;
                      }
                  });
              };
  
              // FILTRO MOBILE - INSTITUCIONAL
              if($('.central_atendimento aside').length > 0){
                  $('aside').css('height', height-101);
                  $('.abre-filtro').click(function(){
                      if(filtro == 1){
                          $('aside').css('display', 'none');
                          $('body').css('position', 'relative');
                          filtro--;
                      }else{
                          $('aside').css('display', 'block');
                          $('body').css('position', 'fixed');
                          filtro++;
                      }
                  });
              };
          });
      }
      else
      {
         console.log('nÃƒÆ’Ã‚Â£o mobile');
      }
  });
  
  // alt 22.04
  
  $(document).ready(function() {
  
  
          $(".full.title").dotdotdot({
              height: 55,
              fallbackToLetter: true,
              watch: true,
          });
  
  
  });