<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=utf-8");
ini_set('display_errors', 1);
$url_pedido = '0';
$produto_kit = curl_init();

curl_setopt_array($produto_kit, array(
  CURLOPT_URL => "http://reidacutelaria.vtexcommercestable.com.br/api/catalog_system/pvt/sku/stockkeepingunitbyid/".$_GET['id'],
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "accept: application/json",
    "cache-control: no-cache",
    "content-type: application/json",
    "postman-token: 23236f97-8417-9cdf-b1cc-18ec7a01639a",
    "x-vtex-api-appkey: vtexappkey-reidacutelaria-VWXMPO",
    "x-vtex-api-apptoken: RGXLFAXMSFJOPDVQFCSCDYMXARYOALRJEUJMECPWIUWPTMMHFSIVBUGSTAKNCIMHOZKUUJMDNWTJBWEOFRJHDVIKOQSERBUZIPBTCRNWGEIEAWRXWWILZFDSRZGDQILL"
  ),
));

$response = curl_exec($produto_kit);
$err = curl_error($produto_kit);

curl_close($produto_kit);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  $dados = json_decode($response);
  if(count($dados->KitItems) > 1){
    $lista = array();
    $valor_final = 0;
    foreach ($dados->KitItems as $produtos) {
      $quantidade = $produtos->Amount;
      if($url_pedido == '0'){
        $url_pedido = 'sku='.$produtos->Id.'&qty='.$produtos->Amount.'&seller=1&';
      }else{
        $url_pedido = $url_pedido.'sku='.$produtos->Id.'&qty='.$produtos->Amount.'&seller=1&';
      }
      
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "http://rnb.vtexcommercestable.com.br/api/pricing/pvt/price-sheet/".$produtos->Id."/?an=reidacutelaria",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "cache-control: no-cache",
          "content-type: application/json",
          "postman-token: af922361-7743-3bbd-2956-a4213a22adf1",
          "x-vtex-api-appkey: vtexappkey-reidacutelaria-VWXMPO",
          "x-vtex-api-apptoken: RGXLFAXMSFJOPDVQFCSCDYMXARYOALRJEUJMECPWIUWPTMMHFSIVBUGSTAKNCIMHOZKUUJMDNWTJBWEOFRJHDVIKOQSERBUZIPBTCRNWGEIEAWRXWWILZFDSRZGDQILL"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        $dados = json_decode($response);
        $valor_final += $dados[0]->listPrice*$produtos->Amount;
      }



      $produto_interno = curl_init();
      curl_setopt_array($produto_interno, array(
        CURLOPT_URL => "http://reidacutelaria.vtexcommercestable.com.br/api/catalog_system/pvt/sku/stockkeepingunitbyid/".$produtos->Id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "accept: application/json",
          "cache-control: no-cache",
          "content-type: application/json",
          "postman-token: 23236f97-8417-9cdf-b1cc-18ec7a01639a",
          "x-vtex-api-appkey: vtexappkey-reidacutelaria-VWXMPO",
          "x-vtex-api-apptoken: RGXLFAXMSFJOPDVQFCSCDYMXARYOALRJEUJMECPWIUWPTMMHFSIVBUGSTAKNCIMHOZKUUJMDNWTJBWEOFRJHDVIKOQSERBUZIPBTCRNWGEIEAWRXWWILZFDSRZGDQILL"
        ),
      ));

      $response = curl_exec($produto_interno);
      $err = curl_error($produto_interno);

      curl_close($produto_interno);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        $produto_final = json_decode($response);
        $tamanho_imagem = '-150-150';
        $imagem_inicial = $produto_final->ImageUrl;
        $imagem_final = str_replace($tamanho_imagem, "-380-380", $imagem_inicial);
        $html = "<div class='imagem_kit kit ativo' data-img='".$imagem_final."''><div class='select_kit'></div><div class='image left'><img src='".$imagem_final."' /></div><div class='right'><h3 class='full title'>'".$produto_final->NameComplete."'</h3><p>Cód. Produto: '".$produto_final->AlternateIdValues[0]."'</p></div></div>";
        $lista['itens'][] = array(
          "html" => $html, 
          "id" => $produto_final->ProductId,
          'url' => $url_pedido,
          'quantidade' => $quantidade
        ); 
      }
    }
  };
  $lista['final'] = $valor_final;
  $objeto = json_encode($lista);
  print_r($objeto);
}
